﻿using UnityEngine;
using System.Collections;

public class tower : MonoBehaviour {
	bool canshoot=true;
	public float reloadtime;
	public GameObject bullet;
	public float bulletspeed = 7;
	public int cost;
	SpriteRenderer sp;
	// Use this for initialization
	void Start () {
		 sp= GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.parent.gameObject.Equals(variables.selectedTower)) {
			sp.enabled= true;
		}else
			sp.enabled= false;
	}

	void OnTriggerStay2D(Collider2D other) {

		if(canshoot && other.tag =="creep"){
			StartCoroutine (Wait());
			canshoot= false;
			GameObject temp= (GameObject)Instantiate((Object)bullet,transform.position,Quaternion.identity);
			temp.GetComponent<Rigidbody2D>().velocity = Vector3.Normalize(other.transform.position-transform.position)*bulletspeed;
	}

			
	}
	IEnumerator Wait(){
		yield return new WaitForSeconds(0.5f/reloadtime);
		canshoot = true;
	}
}
