﻿using UnityEngine;
using System.Collections;

public class Cards : MonoBehaviour {
	public GameObject template;
	public Transform[] handPos;
	public static ArrayList[] hands = {new ArrayList (), new ArrayList ()};
	ArrayList[] decks = {new ArrayList (), new ArrayList ()};
	// Use this for initialization
	void Start () {
		Screen.SetResolution (525, 700, false);
		ArrayList cards = new ArrayList();
		cards.Add (new CardData ("Basic",0, 20, 10, 3, 2));
		cards.Add (new CardData ("Basic II",3, 50, 15,2,5));
		cards.Add (new CardData ("Sniper",1, 60,25,1,8));
		cards.Add (new CardData ("Fast",2, 60,15,5,3));
		foreach (ArrayList deck in decks) {
			foreach (CardData card in cards) {

				for (int i =0; i<5; i++) {
					deck.Add (card);
				}


			}
		}

		Draw ();
		variables.candraw = true;
		Draw ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Draw(){
		if (!variables.battlephase && variables.candraw) {
			for (int j=0; j<2; j++) {
				for (int i =0; i<2; i++) {
					GameObject temp = (GameObject)Instantiate ((Object)template, handPos[j].position, Quaternion.identity);
					if(j==1){
						temp.transform.Rotate(new Vector3(0,0,180));
					}
					int cardID = Random.Range (0, decks[j].Count);
					CardData drawn = (CardData)decks[j][cardID];
					temp.GetComponent<TextMesh> ().text = drawn.name + "\n\nDamage: " + drawn.damage + "\nRate of fire: " +
						drawn.rof + "\nRange: " + drawn.range + "\nCost: " + drawn.cost;
					temp.gameObject.tag = "Card" + drawn.type.ToString ()+j.ToString();
					decks[j].RemoveAt (cardID);
					variables.candraw = false;
					Debug.Log ("Cards in deck: " + decks[j].Count);

					hands[j].Add (temp);

				}
				reOrganize ();
			}
		}
	}
	public void reOrganize(){
		for(int j=0;j<2;j++) {
			for (int i=0; i<hands[j].Count; i++) {
				GameObject temp = (GameObject)hands[j][i];
				Vector3 v = temp.transform.position;
				v.x = handPos[j].position.x + hands[j].Count - 1.5f * i;
				temp.transform.position = v;


			}
		}
	}
}
public class CardData: MonoBehaviour{
	public int cost;
	public int range;
	public int rof;
	public int type;
	public float damage;
			public string name;
	public CardData(string name,int type,int cost,int range, int rof, float damage){
		this.name= name;
		this.cost = cost;
		this.damage = damage;
		this.rof = rof;
		this.cost = cost;
		this.type= type;

	}

}