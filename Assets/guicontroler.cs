﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class guicontroler : MonoBehaviour {
	public Text lives1;
	public Text creepcount1;
	public Text money1;
	public Text lives2;
	public Text creepcount2;
	public Text money2;
	public Text currentLVL;
	public Text Gameover;
	public GameObject menu;
	float timeleft= 10f;
	// Use this for initialization
	void Start () {
	
	}
	public void Exit(){
		Application.Quit ();
	}
	public void Restart(){
		for (int i=0; i<2; i++) {
			variables.lives[i] = 10;
			variables.money[i] = 200;
			variables.creepcount[i] = 0;
		}
		variables.battlephase = false;
		variables.currentlvl = 1;
		Application.LoadLevel (Application.loadedLevel);
		variables.candraw = true;
		
	}
	// Update is called once per frame
	void Update () {
		if (!variables.battlephase) {
			Gameover.GetComponent<Text>().enabled = true;
			Gameover.text ="Time to round: "+((int)timeleft).ToString();
			timeleft-=Time.deltaTime;
			if(timeleft<=0){
				timeleft =10f;
				GetComponent<Spawn>().Spawner();
				Gameover.GetComponent<Text>().enabled= false;
			}
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if(menu.activeSelf){
			menu.SetActive (false);
				Time.timeScale =0.0f;}
			else{
				menu.SetActive (true);
				Time.timeScale =1.0f;
			}
		}
		if (variables.lives[0] < 1) {
			menu.SetActive(true);
			Gameover.GetComponent<Text>().enabled= true;
			Gameover.text = "Player 2 wins!";
			Time.timeScale =1.0f;
		}
		if (variables.lives[1] < 1) {
			menu.SetActive(true);
			Gameover.GetComponent<Text>().enabled= true;
			Gameover.text = "Player 1 wins!";
			Time.timeScale =1.0f;
		}
		if (variables.creepcount[0] == 0 &&variables.creepcount[1] == 0&& variables.battlephase == true) {
			variables.battlephase=false;
			variables.candraw = true;
			variables.currentlvl++;
			currentLVL.text= "Current level: " +variables.currentlvl;
			GetComponent<Cards>().Draw();
		}

		lives1.text = "Lives: " + variables.lives[0];
		creepcount1.text = "Creeps: " + variables.creepcount[0];
		money1.text = "Coins: " + variables.money[0];
		lives2.text = "Lives: " + variables.lives[1];
		creepcount2.text = "Creeps: " + variables.creepcount[1];
		money2.text = "Coins: " + variables.money[1];

	}
}
