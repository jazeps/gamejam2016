﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {
	public GameObject creep1;
	public GameObject creep2;
	public Transform start1;
	public Transform start2;
	public Transform finish;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Spawner(){
		if (variables.battlephase == false) {

			StartCoroutine (Spawnloop ());

		}
	}
	IEnumerator Spawnloop(){
		variables.battlephase = true;

		for (int i=0; i<10; i++) {
			Object temp= Instantiate((Object)creep1,start1.position,Quaternion.identity);
			variables.creepcount[0]++;
			temp= Instantiate((Object)creep2,start2.position,Quaternion.identity);
			variables.creepcount[1]++;
			yield return new WaitForSeconds(0.3f);
		}

	}
}
