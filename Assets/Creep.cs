﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Creep : MonoBehaviour {
	public Transform finish;
	public Transform[] path;
	public float speed =10;
	public float HP= 10;
	public int whichcreep;
	int curpoint=0;
	public int coins;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {

		transform.position = Vector3.MoveTowards(transform.position, path[curpoint].position, Time.deltaTime*speed);
		if (Vector3.Distance (transform.position, path[curpoint].position) < 0.05f) {
			curpoint++;
			
		}
		if (Vector3.Distance (transform.position, finish.position) < 0.05f) {

			variables.lives[whichcreep]--;
			Death();

		}
	}
	void Death(){
		variables.creepcount[whichcreep]--;

		Destroy (this.gameObject);
	}
	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.tag == "bullet") {
			Destroy (coll.gameObject);
			HP-=3;
			if (HP+variables.currentlvl*12<= 0){
				Death();
				variables.money[whichcreep] += coins;
			}
		}
	}
}
