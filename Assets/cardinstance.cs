﻿using UnityEngine;
using System.Collections;

public class cardinstance : MonoBehaviour {
	public GameObject selected;
	SpriteRenderer sp;
	bool hasmoved= false;
	// Use this for initialization
	void Start () {
		sp = GetComponentInChildren<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		try{
			if (gameObject.Equals(variables.selectedCard[0] || variables.selectedCard[1])) 
			selected.SetActive(true);
		else
				selected.SetActive( false);
		}catch{

		}
	
	}
	void OnMouseEnter() {
		if (!hasmoved) {
			Vector3 scale = transform.localScale;
			scale = new Vector3 (0.75f, 0.75f, 0.75f);
			transform.localScale = scale;
			selected.SetActive (true);
			sp.sortingOrder = -1;
			Vector3 pos = transform.position;
			if (transform.rotation.eulerAngles.z > 90)
				pos.y -= 1.1f;
			else
				pos.y += 1.1f;
			transform.position = pos;
			hasmoved= true;
		}

	}
	void OnMouseExit() {

		shrink ();
	}
	public void shrink(){
		if (hasmoved) {
			hasmoved = false;
			Vector3 scale = transform.localScale;
			scale = new Vector3 (0.3f, 0.3f, 0.3f);
			transform.localScale = scale;
			Vector3 pos = transform.position;
			if (transform.rotation.eulerAngles.z > 90)
				pos.y += 1.1f;
			else
				pos.y -= 1.1f;

			transform.position = pos;
			selected.SetActive (false);
			sp.sortingOrder = -2;
		}
	}
}
